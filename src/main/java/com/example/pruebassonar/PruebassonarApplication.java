package com.example.pruebassonar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebassonarApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebassonarApplication.class, args);
	}

}
